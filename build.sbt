lazy val pio = ProjectRef(uri("https://gitlab.com/srnb/playerioscala.git"), "root")

lazy val root = (project in file("."))
  .settings(
    name         := "catcats",
    version      := "0.1",
    scalaVersion := "2.12.6"
  )
  .dependsOn(pio)
