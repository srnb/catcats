package tf.bug.catcats

import cats.effect.IO
import cats.implicits._
import com.playerio.Message
import javax.swing.JOptionPane
import tf.bug.catcats
import tf.bug.catcats.event._
import tf.bug.playerio.{Connection, Game, RoomInfo}

import scala.collection.mutable

object Cat {

  def create(userId: String, username: String, selector: Array[RoomInfo] => RoomInfo)(
    eventHandler: (Cat, Event) => Unit): IO[Cat] = apply(userId, username, selector, eventHandler)

  def apply(
    userId: String,
    username: String,
    selector: Array[RoomInfo] => RoomInfo,
    eventHandler: (Cat, Event) => Unit = (_, _) => {}): IO[Cat] =
    IO {
      Game.load()
      Game(gameId)
    }.flatMap(_.authenticate(Map("userId" -> userId)))
      .flatMap { client =>
        (IO(client), client.multiplayer.rooms(roomType, 50)).tupled
      }
      .flatMap {
        case (client, rooms) =>
          (IO(client), IO(selector(rooms))).tupled
      }
      .flatMap {
        case (client, room) =>
          client.multiplayer.join(room.id, Map("userId" -> userId))
      }
      .flatMap { c =>
        Cat(c, username, eventHandler)
      }

  def apply(connection: Connection, username: String, eventHandler: (Cat, Event) => Unit): IO[Cat] = {
    IO[Cat] {
      val c = new Cat(connection, eventHandler)
      c.name_=(username)
      c
    }
  }

  val defaultHandlers: (Cat, Event) => Unit = (c: Cat, e: Event) => {
    e match {
      case ChatJoin => c.send(ChatJoinResponse(c.username))
      case Movement(p, (x, y), d) => c.mplayers.put(p.number, (p, (x, y, d)))
      case _ =>
    }
  }

  val gamestateHandler: (Cat, Message) => Unit = (c: Cat, m: Message) => {
    println("gs")
    println(m)
    val numplayers = m.getInt(0)
    (0 until numplayers).foreach { n =>
      val name = m.getString((n * 3) + 1)
      val dog = m.getInt((n * 3) + 3)
      val position = c.mplayers.get(n).map(_._2).getOrElse((-1, -1, Direction.Up))
      c.mplayers.put(n, (Player(n, name, dog > 0), position))
    }
    c.ready = true
  }

  val loggingHandler: Message => Unit = m => println(m)

}

class Cat(val conn: Connection, eventHandler: (Cat, Event) => Unit) {

  type Position = (Int, Int, Direction)

  private var ready: Boolean = false
  private var active: Boolean = false
  private var playerNum: Int = -1
  private var username: String = ""
  private var position: Position = (-1, -1, Direction.Up)
  private val mplayers: mutable.Map[Int, (Player, Position)] = mutable.Map()

  def players: Map[Int, (Player, Position)] = mplayers.toMap

  conn.messageListeners += ("chatjoin3", m => {
    if (!active && m.getString(1) == username) {
      playerNum = m.getInt(2)
      println(s"I am $playerNum")
      messageAll(MessageType.GlobalConsole, s"$username entered the game")
    }
    active = true
  })
  conn.messageListeners += ("getgs", Cat.gamestateHandler.curried(this))
  conn.messageListeners += ("gamestate", Cat.gamestateHandler.curried(this))
  conn.messageListeners += ("*", m => {
    val event: Option[Event] = m.getType match {
      case "ChatJoin" => Some(ChatJoin)
      case "c" => Some(catcats.event.Message(MessageType.from(m.getString(2)), m.getString(3), Screen.all(m.getInt(1))))
      case "m" if ready =>
        Some(Movement(players(m.getInt(0))._1, (m.getInt(2), m.getInt(3)), Direction.from(m.getInt(4))))
      case "becomedog" if ready => Some(BecomeDog(players(m.getInt(1))._1))
      case _ => None
    }
    Cat.loggingHandler(m)
    event match {
      case Some(e) =>
        Cat.defaultHandlers(this, e)
        eventHandler(this, e)
      case None =>
    }
  })

  def send(e: Event): Unit = conn ++ (e.code, e.args: _*)

  def player: Int = playerNum

  def name: String = username

  def name_=(name: String): Unit = {
    username = name
  }

  def pos: Position = position

  def pos_=(x: Int, y: Int, direction: Direction): Unit = {
    position = (x, y, direction)
    send(Movement(Player(player, name, false), (x, y), direction)) // TODO store my player state in an object
  }

  def becomeDog(): Unit = send(BecomeDog(Player(player, name, false)))

  def becomeCat(): Unit = conn ++ ("becomecat", name, new Integer(player))

  def message(t: MessageType, screens: List[Screen], m: String): Unit = {
    screens.foreach { s =>
      send(event.Message(t, m, s))
    }
  }

  def messageAll(t: MessageType, m: String): Unit = message(t, Screen.all, m)

  def note(n: Note, screens: List[Screen]): Unit =
    message(MessageType.Broadcast, screens, s"${name.toUpperCase} plays ${n.name}")

}
