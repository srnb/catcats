package tf.bug.catcats

sealed trait Direction {

  val toN: Int

}

object Direction {

  def from(i: Int): Direction = i % 4 match {
    case 0 => Up
    case 1 => Down
    case 2 => Left
    case 3 => Right
  }

  case object Up extends Direction {
    override val toN: Int = 0
  }
  case object Right extends Direction {
    override val toN: Int = 3
  }
  case object Down extends Direction {
    override val toN: Int = 1
  }
  case object Left extends Direction {
    override val toN: Int = 2
  }

}
