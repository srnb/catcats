package tf.bug.catcats

sealed trait Note {

  val name: String

}

object Note {

  def map = Map(
    0 -> LowC,
    1 -> LowD,
    2 -> LowE,
    3 -> LowF,
    4 -> LowG,
    5 -> LowA,
    6 -> LowB,
    7 -> HighC
  )

  case object LowC extends Note {
    override val name: String = "C"
  }

  case object LowD extends Note {
    override val name: String = "D"
  }

  case object LowE extends Note {
    override val name: String = "E"
  }

  case object LowF extends Note {
    override val name: String = "F"
  }

  case object LowG extends Note {
    override val name: String = "G"
  }

  case object LowA extends Note {
    override val name: String = "A"
  }

  case object LowB extends Note {
    override val name: String = "B"
  }

  case object HighC extends Note {
    override val name: String = "high C"
  }

}
