package tf.bug.catcats

case class Player(number: Int, name: String, isDog: Boolean)
