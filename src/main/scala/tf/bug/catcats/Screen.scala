package tf.bug.catcats

sealed trait Screen {

  val number: Int

}

object Screen {

  def all: List[Screen] =
    Error0 ::
      Error1 ::
      BackAlley ::
      DogAltar ::
      Error4 ::
      ATree ::
      NorthernForest ::
      AlleyWestSide ::
      AlleyEastSide ::
      Error9 ::
      FountainRoom ::
      Forest ::
      TheHouse ::
      CaveEntrance ::
      TreasureCave ::
      Error15 ::
      ForestCave ::
      TheMushroom ::
      TwistyCavern ::
      Error19 ::
      Error20 ::
      Error21 ::
      TheDepths ::
      MusicRoom ::
      Error24 :: Nil

  case object Error0 extends Screen {
    override val number: Int = 0
  }

  case object Error1 extends Screen {
    override val number: Int = 1
  }

  case object BackAlley extends Screen {
    override val number: Int = 2
  }

  case object DogAltar extends Screen {
    override val number: Int = 3
  }

  case object Error4 extends Screen {
    override val number: Int = 4
  }

  case object ATree extends Screen {
    override val number: Int = 5
  }

  case object NorthernForest extends Screen {
    override val number: Int = 6
  }

  case object AlleyWestSide extends Screen {
    override val number: Int = 7
  }

  case object AlleyEastSide extends Screen {
    override val number: Int = 8
  }

  case object Error9 extends Screen {
    override val number: Int = 9
  }

  case object FountainRoom extends Screen {
    override val number: Int = 10
  }

  case object Forest extends Screen {
    override val number: Int = 11
  }

  case object TheHouse extends Screen {
    override val number: Int = 12
  }

  case object CaveEntrance extends Screen {
    override val number: Int = 13
  }

  case object TreasureCave extends Screen {
    override val number: Int = 14
  }

  case object Error15 extends Screen {
    override val number: Int = 15
  }

  case object ForestCave extends Screen {
    override val number: Int = 16
  }

  case object TheMushroom extends Screen {
    override val number: Int = 17
  }

  case object TwistyCavern extends Screen {
    override val number: Int = 18
  }

  case object Error19 extends Screen {
    override val number: Int = 19
  }

  case object Error20 extends Screen {
    override val number: Int = 20
  }

  case object Error21 extends Screen {
    override val number: Int = 21
  }

  case object TheDepths extends Screen {
    override val number: Int = 22
  }

  case object MusicRoom extends Screen {
    override val number: Int = 23
  }

  case object Error24 extends Screen {
    override val number: Int = 24
  }

}
