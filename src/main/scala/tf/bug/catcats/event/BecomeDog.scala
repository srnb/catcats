package tf.bug.catcats.event

import tf.bug.catcats.Player

case class BecomeDog(player: Player) extends Event {
  override val code: String = "becomedog"

  override def args: List[AnyRef] = player.name :: new Integer(player.number) :: Nil
}
