package tf.bug.catcats.event

case object ChatJoin extends Event {
  override val code: String = "ChatJoin"

  override def args: List[AnyRef] = Nil
}
