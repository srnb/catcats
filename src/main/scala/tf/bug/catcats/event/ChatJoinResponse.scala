package tf.bug.catcats.event

case class ChatJoinResponse(name: String) extends Event {
  override val code: String = "chatjoin2"

  override def args: List[AnyRef] = name :: Nil
}
