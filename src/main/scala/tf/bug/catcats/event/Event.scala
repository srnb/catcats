package tf.bug.catcats.event

trait Event {

  val code: String
  def args: List[AnyRef]

}
