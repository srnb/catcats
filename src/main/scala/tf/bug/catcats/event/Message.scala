package tf.bug.catcats.event

import tf.bug.catcats.Screen

import scala.util.matching.Regex

case class Message(messageType: MessageType, message: String, room: Screen) extends Event {
  override val code: String = "c"

  override def args: List[AnyRef] = new Integer(room.number) :: messageType.key :: message :: Nil
}

trait MessageType {

  val key: String

}

object MessageType {

  val meReg: Regex = """\+(.*)""".r

  def from(s: String): MessageType = {
    s match {
      case "?" => Broadcast
      case "!!" => GlobalConsole
      case meReg(player) => MeMessage(player)
      case p => Normal(p)
    }
  }

  case object Broadcast extends MessageType {
    override val key: String = "?"
  }

  case object GlobalConsole extends MessageType {
    override val key: String = "!!"
  }

  case class MeMessage(playerName: String) extends MessageType {
    override val key: String = s"+$playerName"
  }

  case class Normal(playerName: String) extends MessageType {
    override val key: String = playerName
  }

}
