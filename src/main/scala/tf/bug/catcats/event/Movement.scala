package tf.bug.catcats.event

import tf.bug.catcats.{Direction, Player}

case class Movement(player: Player, position: (Int, Int), direction: Direction) extends Event {
  override val code: String = "m"

  override def args: List[AnyRef] = {
    player.name :: new Integer(player.number) :: new Integer(position._1) :: new Integer(position._2) :: new Integer(
      direction.toN) :: Nil
  }
}
