package tf.bug.catcatstest

import java.util.concurrent.TimeUnit

import cats._
import cats.data._
import cats.implicits._
import cats.effect.IO
import tf.bug.catcats.event.{BecomeDog, Message, MessageType, Movement}
import tf.bug.catcats._

import scala.collection.mutable
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration
import scala.util.Random

object Main {

  val tpReg = """,tp (\d+) (\d+)""".r
  val globReg = """,shout (.+)""".r
  val mcReg = """.+? ,cat""".r

  def main(args: Array[String]): Unit = {
    var players: Seq[Player] = Seq()
    Cat
      .create("dab123", "please hit that upvote", _.filter(_.id.contains("spoo")).head) { (cat, e) =>
        e match {
          case Movement(p, xy, d) =>
            println(s"$p moved to $xy facing $d")
            players = players :+ p
//        case Message(t, c, s) =>
//          println(s"Message of $t with $c at $s")
          case Message(MessageType.Normal(pn), globReg(c), _) =>
            cat.messageAll(MessageType.Broadcast, s"$pn: $c")
          case Message(MessageType.Normal(pn), ",dog", _) =>
            players.find(_.name == pn) match {
              case Some(p) => cat.send(BecomeDog(p))
              case None =>
            }
          case Message(MessageType.MeMessage(pn), mcReg(), _) =>
            players.find(_.name == pn) match {
              case Some(p) => cat.conn ++ ("becomecat", p.name, new Integer(p.number))
              case None =>
            }
          case Message(MessageType.Normal(pn), tpReg(x, y), _) =>
            players.find(_.name == pn) match {
              case Some(p) => cat.send(Movement(p, (x.toInt, y.toInt), Direction.Up))
              case None =>
            }
          case _ =>
        }
      }
      .unsafeRunSync()
  }

}
